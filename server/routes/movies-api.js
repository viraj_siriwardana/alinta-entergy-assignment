const express = require('express');
const axios = require('axios');

var _ = require('underscore');

const router = express.Router();
const BASE_API_URL = 'http://alintacodingtest.azurewebsites.net';

router.get('/movies', (req, res) => {
  axios.get(`${BASE_API_URL}/api/movies`)
    .then((posts) => {
      var movies = posts.data;

      var flatten = [];
      movies.forEach((movie) => {
        var name = movie.name || 'Name Not Found';
        var roles = movie.roles;
        roles.forEach((role) => {
          role.movie = name;
          flatten.push(role);
        })
      });
      res.status(200).json(flatten);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

module.exports = router;